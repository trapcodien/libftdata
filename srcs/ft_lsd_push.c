/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsd_push.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 20:45:16 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 20:47:59 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		ft_lsd_pushfront(t_lsd *lsd, void *new_elsd)
{
	if (lsd)
		ft_lsd_insert_left(lsd, new_elsd, lsd->head);
}

void		ft_lsd_pushback(t_lsd *lsd, void *new_elsd)
{
	if (lsd)
		ft_lsd_insert_right(lsd, lsd->tail, new_elsd);
}
