/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_get_distance.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 02:07:23 by garm              #+#    #+#             */
/*   Updated: 2014/11/26 01:01:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

size_t			ft_cbuf_get_distance(t_cbuf *cbuf, size_t src, size_t dst)
{
	size_t		size;

	if (cbuf)
	{
		size = cbuf->size;
		if (dst == src && cbuf->state == CBUF_FULL)
			return (size);
		else if (dst >= src)
			return ((dst - src));
		else
			return ((size + (dst - src)));
	}
	return (0);
}
