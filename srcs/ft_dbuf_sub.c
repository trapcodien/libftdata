/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_sub.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 00:03:49 by garm              #+#    #+#             */
/*   Updated: 2014/06/24 00:25:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

t_dbuf			*ft_dbuf_sub(t_dbuf *b, long shift, size_t len)
{
	t_dbuf		*new_buf;
	long		start;
	long		i;

	if (len == 0 || !b)
		return (NULL);
	start = b->offset + shift;
	if (!(new_buf = ft_dbuf_create(len)))
		return (NULL);
	new_buf->resize = b->resize;
	i = 0;
	while (i < (long)len)
	{
		if (start < 0 || start >= (long)b->size)
		{
			ft_dbuf_set(new_buf, 0, 0, 1);
			new_buf->offset++;
		}
		else
			ft_dbuf_write(new_buf, (char *)b->data + start, 1);
		i++;
		start++;
	}
	new_buf->offset = 0;
	return (new_buf);
}
