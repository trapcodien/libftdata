/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_destroy.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 20:17:41 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 20:21:44 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		ft_dbuf_destroy(t_dbuf **buffer)
{
	if (buffer && *buffer)
	{
		ft_memdel((void **)&((*buffer)->data));
		ft_memdel((void **)buffer);
	}
}
