/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bst_insert.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 20:54:47 by garm              #+#    #+#             */
/*   Updated: 2015/03/23 22:34:48 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		*bst_insert(void *t, void *n, int (*cmp)())
{
	t_btree		*tree;
	t_btree		*node;

	tree = t;
	node = n;
	if (!tree || !node || tree == node)
		return (node);
	if (cmp(node, tree) <= 0)
	{
		if (tree->left)
			return (bst_insert(tree->left, node, cmp));
		tree->left = node;
	}
	else
	{
		if (tree->right)
			return (bst_insert(tree->right, node, cmp));
		tree->right = node;
	}
	return (node);
}
