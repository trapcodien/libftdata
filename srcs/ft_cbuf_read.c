/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_read.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/27 01:50:57 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 02:35:11 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

ssize_t			ft_cbuf_read(int fd, t_cbuf *cbuf, size_t nbytes)
{
	ssize_t		ret;
	size_t		rbytes;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!nbytes || cbuf->state == CBUF_FULL)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbytes = (*c > *p) ? (*c - *p) : ((off_t)cbuf->size - *p);
	rbytes = (nbytes < rbytes) ? (nbytes) : (rbytes);
	ret = read(fd, ((char *)cbuf->data) + *p, rbytes);
	if (ret <= 0 && rbytes)
		return (ERROR);
	*p += ret;
	if (*p == (off_t)cbuf->size)
		*p = 0;
	cbuf->state = (*p == *c) ? (CBUF_FULL) : (CBUF_NORMAL);
	return (ret);
}
