/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsd_pop.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 20:48:07 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 20:51:00 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		*ft_lsd_popfront(t_lsd *lsd)
{
	void	*head;

	if (!lsd)
		return (NULL);
	head = lsd->head;
	return (ft_lsd_pick(lsd, &head));
}

void		*ft_lsd_popback(t_lsd *lsd)
{
	void	*tail;

	if (!lsd)
		return (NULL);
	tail = lsd->tail;
	return (ft_lsd_pick(lsd, &tail));
}
