/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 01:43:18 by garm              #+#    #+#             */
/*   Updated: 2014/11/26 02:04:12 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void				*ft_stack_create(void *f_construct)
{
	t_data_construct	c;
	void				*stack;

	if ((c = f_construct))
		stack = c();
	else
		stack = ft_memalloc(sizeof(t_stack));
	if (!stack)
		return (NULL);
	((t_stack *)stack)->next = NULL;
	return (stack);
}

void				ft_stack_destroy(void *a_elem, void *f_destruct)
{
	t_data_destruct		d;
	t_stack				**elem;

	elem = (t_stack **)a_elem;
	if (!elem || !*elem)
		return ;
	if ((d = f_destruct))
		d(*elem);
	ft_memdel(a_elem);
}

void				ft_stack_push(void *a_stack, void *new_elem)
{
	t_stack				**stack;
	t_stack				*new;

	if (!a_stack || !new_elem)
		return ;
	stack = (t_stack **)a_stack;
	new = new_elem;
	new->next = *stack;
	*stack = new;
}

void				ft_stack_del(void *a_stack, void *elem, void *f_destruct)
{
	t_stack				**stack;
	t_stack				*cursor;
	t_stack				*prev;

	stack = (t_stack **)a_stack;
	if (!stack || !*stack || !elem)
		return ;
	prev = NULL;
	cursor = *stack;
	while (cursor && cursor != elem && (prev = cursor))
		cursor = cursor->next;
	if (!cursor)
		return ;
	if (prev)
		prev->next = cursor->next;
	else
		(*stack) = (*stack)->next;
	ft_stack_destroy((void **)&cursor, f_destruct);
}

void				ft_stack_pop(void *a_stack, void *f_destruct)
{
	t_stack			**stack;

	stack = (t_stack **)a_stack;
	if (!stack || !*stack)
		return ;
	ft_stack_del(stack, *stack, f_destruct);
}
