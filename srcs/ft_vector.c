/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/08 22:01:39 by garm              #+#    #+#             */
/*   Updated: 2014/12/08 22:53:07 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "libftdata.h"

t_vector		ft_vector(t_point a, t_point b)
{
	t_vector	v;

	v.a = a;
	v.a = b;
	v.x = b.x - a.x;
	v.y = b.y - a.y;
	return (v);
}

int				ft_vector_cmpsize(t_vector v1, t_vector v2)
{
	double		d1;
	double		d2;
	double		diff;

	d1 = sqrt(pow(v1.x, 2) + pow(v1.y, 2));
	d2 = sqrt(pow(v2.x, 2) + pow(v2.y, 2));
	diff = d1 - d2;
	if (diff < 0)
		return (-1);
	else if (diff > 0)
		return (1);
	else
		return (0);
}
