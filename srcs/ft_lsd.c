/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/27 16:50:01 by garm              #+#    #+#             */
/*   Updated: 2014/11/27 16:54:50 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

t_lsd		*ft_lsd_create(void)
{
	return (ft_memalloc(sizeof(t_lsd)));
}

void		ft_lsd_destroy(t_lsd **lsd)
{
	ft_memdel((void **)lsd);
}
