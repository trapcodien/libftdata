# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/02/07 21:16:03 by garm              #+#    #+#              #
##   Updated: 2015/03/23 22:20:51 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = libftdata.a

SOURCES_DIR = srcs
INCLUDES_DIR = includes

CHECK = @cppcheck -I $(INCLUDES_DIR) --enable=all --check-config --suppress=missingIncludeSystem .

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -std=c89
endif

CFLAGS =  $(FLAGS) -I $(INCLUDES_DIR) -I ./$(LIB_DIR)/$(INCLUDES_DIR)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/libftdata.h

SOURCES = \
			 $(SOURCES_DIR)/ft_cbuf_create.c \
			 $(SOURCES_DIR)/ft_cbuf_destroy.c \
			 $(SOURCES_DIR)/ft_cbuf_read.c \
			 $(SOURCES_DIR)/ft_cbuf_produce.c \
			 $(SOURCES_DIR)/ft_cbuf_consume.c \
			 $(SOURCES_DIR)/ft_cbuf_get_distance.c \
			 $(SOURCES_DIR)/ft_cbuf_findchr.c \
			 $(SOURCES_DIR)/ft_cbuf_debug.c \
			 $(SOURCES_DIR)/ft_stack.c \
			 $(SOURCES_DIR)/ft_stack_debug.c \
			 $(SOURCES_DIR)/ft_queue.c \
			 $(SOURCES_DIR)/ft_lsd.c \
			 $(SOURCES_DIR)/ft_lsd_insert.c \
			 $(SOURCES_DIR)/ft_lsd_pick.c \
			 $(SOURCES_DIR)/ft_lsd_push.c \
			 $(SOURCES_DIR)/ft_lsd_pop.c \
			 $(SOURCES_DIR)/ft_htable_create.c \
			 $(SOURCES_DIR)/ft_htable_destroy.c \
			 $(SOURCES_DIR)/ft_htable_hash.c \
			 $(SOURCES_DIR)/ft_htable_getdata.c \
			 $(SOURCES_DIR)/ft_htable_setdata.c \
			 $(SOURCES_DIR)/ft_dbuf_create.c \
			 $(SOURCES_DIR)/ft_dbuf_destroy.c \
			 $(SOURCES_DIR)/ft_dbuf_write.c \
			 $(SOURCES_DIR)/ft_dbuf_write_str.c \
			 $(SOURCES_DIR)/ft_dbuf_set.c \
			 $(SOURCES_DIR)/ft_dbuf_bzero.c \
			 $(SOURCES_DIR)/ft_dbuf_sub.c \
			 $(SOURCES_DIR)/ft_dbuf_dup.c \
			 $(SOURCES_DIR)/ft_point.c \
			 $(SOURCES_DIR)/ft_vector.c \
			 $(SOURCES_DIR)/ft_bst_insert.c \
			 $(SOURCES_DIR)/ft_bst_foreach.c \
			 $(SOURCES_DIR)/ft_bst_destroy.c \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@ar rcs $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

cleanbin:
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

check:
	$(CHECK)

fclean: clean cleanbin
	@echo Full clean OK.

re: fclean all

.PHONY: clean cleanbin fclean re all

